<?php
namespace Website\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class TranslatesFixture extends TestFixture {

/**
 * table property
 *
 * @var string
 */
  public $table = 'i18n';

/**
 * fields property
 *
 * @var array
 */
  public $fields = array(
    'id' => ['type' => 'integer'],
    'locale' => ['type' => 'string', 'length' => 6, 'null' => false],
    'model' => ['type' => 'string', 'null' => false],
    'foreign_key' => ['type' => 'integer', 'null' => false],
    'field' => ['type' => 'string', 'null' => false],
    'content' => ['type' => 'text'],
    '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]],

  );

/**
 * records property
 *
 * @var array
 */
  public $records = array(
    array('locale' => 'spa', 'model' => 'Sites', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Mi Sitio'),
    array('locale' => 'eng', 'model' => 'Sites', 'foreign_key' => 1, 'field' => 'title', 'content' => 'My Site'),

   
  );
}