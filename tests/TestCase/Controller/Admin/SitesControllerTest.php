<?php
namespace Website\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Website\Controller\Admin\SitesController;
use Cake\Core\Configure;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;

/**
 * Website\Controller\Admin\SitesController Test Case
 */
class SitesControllerTest extends IntegrationTestCase
{

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
    'Sites' => 'plugin.website.sites',
    'Sections' => 'plugin.section.sections',
    'Layouts' => 'plugin.section.layouts',
    'Wrappers' => 'plugin.section.wrappers',
    'I18n' => 'plugin.website.translates',
    'plugin.i18n.languages',

  ];  


  /**
 * setUp method
 *
 * @return void
 */
  public function setUp()  
  {
    parent::setUp();
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    unset( $this->Languages);
    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  public function testDefault()
  {
    
  }

/**
 * Test para comprobar que cuando el web no es multisite en el controller siempre se va a devolver el mismo id, 
 * aunque no se pasen argumentos
 */
  public function _testUpdateMonosite()
  {
    Configure::write( 'App.multisite', false);
    $this->setLanguages();
    $response = $this->getMock('Cake\Network\Response');
    $SitesController = new SitesController( new Request(), $response);
    $SitesController->modelClass = 'Website.Sites';
    $View = $SitesController->createView();
    $SitesController->startupProcess();
    $SitesController->update();

    $site = $SitesController->CrudTool->getContent();
    $this->assertEquals( $site ['id'], 1);
  }

/** 
 * Test para comprobar que cuando el web no es multisite en el controller siempre se va a devolver el mismo id, 
 * aunque no se pasen argumentos
 * Se tiene en cuenta que no hay ningún site creado previamente, con lo que la aplicación tiene que ser capaz de crearlo
 */
  public function _testUpdateMonositeAndCreateSite()
  {
    Configure::write( 'App.multisite', false);
    $this->setLanguages();
    $response = $this->getMock('Cake\Network\Response');
    $SitesController = new SitesController( new Request(), $response);
    $SitesController->modelClass = 'Website.Sites';
    $sites = $SitesController->Sites->find()->select( ['Sites.id'])->toArray();

    // Borra todos los sites
    foreach( $sites as $site)
    {
      $SitesController->Sites->delete( $site);
    }
    
    $View = $SitesController->createView();
    $SitesController->startupProcess();
    $SitesController->update();
    $site = $SitesController->CrudTool->getContent();
    $this->assertTrue( array_key_exists( 'id', $site));
  }

}
