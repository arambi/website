<?php
namespace Website\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Website\Controller\Component\SitesComponent;
use Cake\Controller\Controller;
use Cake\Network\Request;
use Cake\Network\Session;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Website\Controller\Component\SitesComponent Test Case
 */
class SitesComponentTest extends TestCase
{

  public $fixtures = [
    'plugin.website.sites',
    'plugin.website.translates',
    'plugin.i18n.languages',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $registry = new ComponentRegistry();
    $this->SitesComponent = new SitesComponent($registry);
    $this->Controller = new Controller(new Request(['session' => new Session()]));
    $this->SitesComponent->Controller = $this->Controller;
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
    $this->Sites = TableRegistry::get( 'Website.Sites');
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
    $this->Contents->addBehavior( 'I18n.I18nable', ['fields' => ['title', 'body']]);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset($this->SitesComponent);

    parent::tearDown();
  }

  /**
   * Test setFrontDomain method
   *
   * @return void
   */
  public function testDefault()
  {

  }



}
