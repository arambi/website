<?php
namespace Website\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Website\Model\Table\SitesTable;
use Cake\Core\Configure;
use I18n\Lib\Lang;
use Website\Lib\Website;

/**
 * Website\Model\Table\SitesTable Test Case
 */
class SitesTableTest extends TestCase
{

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
    'Sites' => 'plugin.website.sites',
    'plugin.i18n.languages',
    'I18n' => 'plugin.website.translates',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $config = TableRegistry::exists('Sites') ? [] : ['className' => 'Website\Model\Table\SitesTable'];
    $this->Sites = TableRegistry::get('Sites', $config);
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset( $this->Sites);
    unset( $this->Languages);
    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  /**
   * Test initialize method
   *
   * @return void
   */
  public function testSetSite()
  {
    $_SERVER ['HTTP_HOST'] = 'mysite.site';
    $this->Sites->setSite();
    $this->assertInstanceOf( 'Website\Model\Entity\Site', Website::get());
    $this->assertEquals( Website::get()->slug, 'mysite');
  }

  public function testCreateDefaultSite()
  {
    $this->setLanguages();
    $site = $this->Sites->createDefaultSite();
    $this->assertInstanceOf( 'Website\Model\Entity\Site', $site);
  }

}
