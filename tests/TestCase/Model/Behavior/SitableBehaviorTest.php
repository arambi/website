<?php
namespace Website\Test\TestCase\Model\Behavior;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class ContentsTable extends Table {

	public function initialize(array $options) {
		$this->addBehavior( 'Website.Sitable');
	}
}
/**
 * Website\Model\Behavior\SitableBehavior Test Case
 */
class SitableBehaviorTest extends TestCase {


	public $fixtures = [
		'plugin.website.contents',
		'plugin.website.sites',
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->connection = ConnectionManager::get('test');
		$this->Contents = new ContentsTable([
			'alias' => 'Contents',
			'table' => 'contents',
			'connection' => $this->connection
		]);
		$this->Sites = TableRegistry::get( 'Website.Sites');
		$_SERVER ['HTTP_HOST'] = 'mysite.site';
    $this->Sites->setSite();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contents);
		unset($this->Sites);
		parent::tearDown();
	}


	public function testGet()
	{
		$query = $this->Contents->find();
		$ids =  $query->all()->extract( 'site_id')->toArray();
		$ids = array_unique( $ids);
		$this->assertEquals( count( $ids), 1);
	}

	public function testSave()
	{
		$site_id = 1;

		$content = $this->Contents->newEntity([
			'title' => 'Hello word'
		]);

		if( $this->Contents->save( $content))
		{
			$this->assertEquals( $content->get( 'site_id'), $site_id);
		}
		else
		{
			$this->assertTrue( false);
		}
	}


}
