<?php
/**
 * Test suite bootstrap for Website.
 */
// Customize this to be a relative path for embedded plugins.
// For standalone plugins, this should point at a CakePHP installation.
require '../../config/bootstrap.php';
