<?php

$config ['Access'] = [
  'sites' => [
    'name' => __d( 'admin', 'Configuración del Web'),
    'options' => [
      'update' => [
        'name' => __d( 'admin', 'Editar'),
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'Website',
            'controller' => 'sites',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Website',
            'controller' => 'sites',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Website',
            'controller' => 'sites',
            'action' => 'update',
          ],
        ],
      ]
    ]
  ]
];