<?php
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

$connection = ConnectionManager::get('default');

foreach( ['sites'] as $table)
{
	$results = $connection->execute( "SHOW TABLES LIKE '$table'")->fetchAll('assoc');
	
  if( empty( $results))
  {
    return;
  }
}

TableRegistry::get( 'Website.Sites')->setSite();

Router::plugin('Website', function($routes) {
	$routes->fallbacks();
});


Router::connect( '/robots.txt', [
  'plugin' => 'Website',
  'controller' => 'Sites',
  'action' => 'robots'
]);
