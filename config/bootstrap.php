<?php 

use User\Auth\Access;
use Cake\ORM\TableRegistry;
use Cake\Routing\DispatcherFactory;
use Manager\Navigation\NavigationCollection;
use Website\Routing\Filter\LocalBusinessFilter;

// TableRegistry::get( 'Website.Sites')->setSite();


// CONFIGURACIÓN DE LOS MENUS DE ADMINISTRACIÓN
NavigationCollection::add( [
  'name' => 'Configuración',
  'icon' => 'fa fa-gear',
  'url' => false,
  'key' => 'settings'
]);

NavigationCollection::add( [
  'parent' => 'settings',
  'name' => 'General',
  'parentName' => 'Configuración',
  'plugin' => 'Website',
  'controller' => 'Sites',
  'action' => 'update',
  'icon' => 'fa fa-cogs',
]);

NavigationCollection::add( [
  'parent' => 'settings',
  'name' => 'Idiomas',
  'parentName' => 'Configuración',
  'plugin' => 'I18n',
  'controller' => 'Languages',
  'action' => 'index',
  'icon' => 'fa fa-flag',
]);


Access::add( 'sites', [
  'name' => __d( 'admin', 'Configuración del Web'),
  'options' => [
    'update' => [
      'name' => __d( 'admin', 'Editar'),
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Website',
          'controller' => 'sites',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Website',
          'controller' => 'sites',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Website',
          'controller' => 'sites',
          'action' => 'update',
        ],
      ],
    ]
  ]
]);


DispatcherFactory::add( new LocalBusinessFilter(['priority' => 2]));
