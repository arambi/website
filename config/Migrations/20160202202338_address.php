<?php

use Phinx\Migration\AbstractMigration;

class Address extends AbstractMigration
{
  public function up()
  {
    $contents = $this->table( 'sites');
    $contents
      ->addColumn( 'address', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'analytics', 'string', ['default' => NULL, 'null' => true])
      ->update();
  }
  
  public function down()
  {
    $contents = $this->table( 'sites');
    $contents->removeColumn( 'address')->update();
    $contents->removeColumn( 'analytics')->update();
  }
}
