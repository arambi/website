<?php

use Phinx\Migration\AbstractMigration;

class Dates extends AbstractMigration
{
  public function change()
  {
    $contents = $this->table( 'sites');
    $contents
      ->addColumn( 'date_post', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_post_short', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_event_punctual', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_event_permanent', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_event_permanent_expr', 'string', ['default' => NULL, 'null' => true])
      ->save();
  }
}
