<?php
use Migrations\AbstractMigration;

class SitesEmailLong extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
      $sites = $this->table( 'sites');
      $sites
          ->changeColumn( 'email', 'string', ['limit' => 128, 'default' => NULL, 'null' => true])
          ->update();
  }
}
