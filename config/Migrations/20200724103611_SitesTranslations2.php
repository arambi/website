<?php
use Migrations\AbstractMigration;

class SitesTranslations2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $sites = $this->table( 'sites');

        if( !$sites->hasColumn('horario')) {
            $sites
                ->addColumn( 'horario', 'text', ['default' => NULL, 'null' => true])
                ->update();
        }
        
        $sites = $this->table( 'sites_translations');

        if( !$sites->hasColumn('address')) {
           $sites
            ->addColumn( 'address', 'text', ['null' => true, 'default' => NULL])
            ->update();
        }

        if( !$sites->hasColumn('horario')) {
           $sites
            ->addColumn( 'horario', 'text', ['null' => true, 'default' => NULL])
            ->update();
        }
    }

    
}
