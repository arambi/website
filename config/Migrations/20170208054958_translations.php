<?php

use Phinx\Migration\AbstractMigration;

class Translations extends AbstractMigration
{
  public function up()
  {
    $letters = $this->table( 'sites_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $letters
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => false, 'limit' => 255])
      ->addColumn( 'subtitle', 'string', ['null' => false, 'limit' => 255])
      ->addColumn( 'description', 'text', ['null' => true, 'default' => NULL])
      ->addColumn( 'date_post', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_post_short', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_event_punctual', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_event_permanent', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'date_event_permanent_expr', 'string', ['default' => NULL, 'null' => true])
      ->save();  
  }

  public function down()
  {
    $this->dropTable( 'sites_translations');
  }

}
