<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     * 
    public function change()
    {
        $this->table( 'sites')->addColumn( 'googlefonts', 'text', ['default' => NULL, 'null' => true])->update();
    }
   */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
      // Tabla para los models de contenido
      $sites = $this->table( 'sites');
      $sites
            ->addColumn( 'slug', 'string', ['null' => true, 'default' => NULL, 'limit' => 255])
            ->addColumn( 'domain', 'string', ['null' => true, 'default' => NULL, 'limit' => 60])
            ->addColumn( 'title', 'string', ['null' => false, 'limit' => 255])
            ->addColumn( 'description', 'text', ['null' => true, 'default' => NULL])
            ->addColumn( 'subtitle', 'string', ['null' => false, 'limit' => 255])

            // Json, donde se guardarán preferencias
            ->addColumn( 'settings', 'text', ['default' => NULL, 'null' => true])

            // Para cuestiones de seguridad
            ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])

            // Logo en JSON
            ->addColumn( 'logo', 'text', ['default' => NULL, 'null' => true])

            // Favicon
            ->addColumn( 'favicon', 'string', ['default' => NULL, 'null' => true])

            // Email
            ->addColumn( 'email', 'string', ['limit' => 36, 'default' => NULL, 'null' => true])

            // Theme
            ->addColumn( 'theme', 'string', ['limit' => 36, 'default' => NULL, 'null' => true])

            ->addColumn( 'googlefonts', 'text', ['default' => NULL, 'null' => true])

            // SEO
            ->addColumn( 'homepage_title_format', 'string', ['default' => NULL, 'null' => true]) 
            ->addColumn( 'section_title_format', 'string', ['default' => NULL, 'null' => true]) 
            ->addColumn( 'item_title_format', 'string', ['default' => NULL, 'null' => true]) 

            ->addColumn( 'created', 'datetime', ['default' => null])
            ->addColumn( 'modified', 'datetime', ['default' => null])

            ->addIndex( ['domain'])
            ->addIndex( ['slug'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'sites');
    }
}