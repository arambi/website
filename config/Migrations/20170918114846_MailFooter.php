<?php
use Migrations\AbstractMigration;

class MailFooter extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $sites = $this->table( 'sites');
    $sites
      ->addColumn( 'mail_footer', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'mail_legal', 'text', ['default' => NULL, 'null' => true])
      ->save();
    
    $sites_translations = $this->table( 'sites_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $sites_translations
      ->addColumn( 'mail_footer', 'text', ['null' => true, 'default' => NULL])
      ->addColumn( 'mail_legal', 'text', ['null' => true, 'default' => NULL])
      ->save();  
  }
}
