User-agent: *
<?php if( $this->Nav->website( 'settings.disallow_robots')): ?>
Disallow: /
<?php else: ?>
Allow: /
<?php endif ?>
<?php foreach( $disallows as $disallow): ?>
Disallow: <?= $disallow ?>

<?php endforeach ?>

Sitemap: <?= \Cake\Routing\Router::url( '/sitemap.xml', true) ?>