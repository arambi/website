<div ng-if="!field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <input cf-seo-titles type="text" ng-model="content[field.key]" class="form-control" />
    <div cf-seo-titles="%s,%c,%i" ng-model="content[field.key]"></div>
    <span class="form-error" ng-if="field.error" ng-repeat="error in field.error">{{ error }}</span>
  </div>
</div>