<div class="form-group">
  <label class="col-sm-2 control-label">{{ field.label }}</label>
  <div class="col-sm-10">
    <div class="input-group">
      <select ng-model="content[field.key]" ng-options="item as item for item in data.crudConfig.googlefonts track by item" multiple chosen class="chosen-select" style="width:350px;" tabindex="4">
      </select>
    </div>
  </div>
</div>
