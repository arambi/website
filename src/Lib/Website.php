<?php 

namespace Website\Lib;


/**
* 
*/
class Website
{
  protected static $_site;

  public static function isSeted()
  {
    return !empty( static::$_site);
  }

  public static function set( $site)
  {
    static::$_site = $site;
  }

  public static function get( $key = false)
  {
    if( empty( static::$_site))
    {
      return null;
    }
    
    if( strpos( $key, '.') !== false)
    {
      list( $key1, $key2) = explode( '.', $key);

      if( isset( static::$_site->$key1->$key2))
      {
        return static::$_site->$key1->$key2;
      }
      else
      {
        return null;
      }
    }

    if( !$key)
    {
      return static::$_site;
    }
    
    return static::$_site->$key;
  }
}