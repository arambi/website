<?php
namespace Website\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use I18n\Lib\Lang;

/**
 * Dumper shell command.
 */
class DumperShell extends Shell
{

  public $tasks = ['Cofree.Installer'];

  /**
   * main() method.
   *
   * @return bool|int Success or error code.
   */
  public function main() 
  {
    $this->createSites();
  }

  public function createSites()
  {
    $Sites = TableRegistry::get( 'Website.Sites');

    $data = [
      'item_title_format' => "%i - %c - %s",
      'section_title_format' => "%c - %s",
      'homepage_title_format' => "%s",
      'description' => 'Es una descripción del sitio web',
      'theme' => 'TestPlugin'
    ];

    $locales = Lang::iso3();
    $sitename = $this->in( 'Nombre el website', null, 'Mi sitio web');

    foreach( $locales as $key => $name)
    {
      $data ['_translations'][$key]['title'] = $sitename ;
      $data ['_translations'][$key]['subtitle'] = $sitename;
    }
    
    $entity = $Sites->newEntity( $data);

    if( $Sites->save( $entity))
    {
      $this->Installer->out( 'El sitio web se ha creado correctamente');
    }
    else
    {
      $this->Installer->out( 'No se ha podido crear el sitio web');
    }

    $Sites->setSite();
  }

}