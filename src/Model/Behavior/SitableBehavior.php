<?php

namespace Website\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Event\Event;
use Cake\Core\Configure;
use ArrayObject;
use Website\Lib\Website;

/**
 * Sitable behavior class
 * 
 * Realiza labores en los registros de los models relacionados con el model Site.
 * 
 * @package Website.Model.Behavior
 */
class SitableBehavior extends Behavior
{
  
  public function beforeSave( Event $event, Entity $entity, ArrayObject $options)
  {
    $Table = $event->subject();

    if( !$entity->has( 'id') 
        && Website::isSeted() 
        && $Table->hasField( 'site_id')
        && !$entity->has( 'site_id'))
    {
      $entity->set( 'site_id', Website::get()->id);
    }
  }
  
/**
 * En `beforeFind()` se asegura de que todas las peticiones a los registros se hacen de forma restrictiva Sitio que se está editando
 * Se añadirá a `$query` las condiciones correctas para esa restricción
 *
 * @param object $Model 
 * @param array $query 
 * @return array El query para la petición de la búsqueda
 */
  public function beforeFind( Event $event, Query $query)
  {    
    $site_id = $this->getSiteId();

    if( $site_id && $event->subject()->hasField( 'site_id'))
    {
      $query->where( ['site_id' => $site_id]);
    }
  }
  

  
/**
 * Devuelve el id del sitio, tomando el parámetro en CakeRequest::params ['site']
 *
 * @param object $Model 
 * @param string $site Permalink del site
 * @return integer || false
 */
  public function getSiteId()
  {
    if( Website::isSeted() )
    {
      return Website::get()->id;
    }
    
    return false;
  }
  
}

?>