<?php

namespace Website\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use I18n\Lib\Lang;
use Website\Lib\Website;

/**
 * Sites Model
 */
class SitesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table('sites');
    $this->displayField('title');
    $this->primaryKey('id');
    $this->addBehavior('Timestamp');

    $this->addBehavior('Cofree.Saltable');

    $this->addBehavior(Configure::read('I18n.behavior'), [
      'fields' => [
        'title',
        'subtitle',
        'description',
        'date_post',
        'date_post_short',
        'date_event_punctual',
        'date_event_permanent',
        'date_event_permanent_expr',
        'mail_footer',
        'mail_legal'
      ]
    ]);

    $this->addBehavior('Cofree.Jsonable', [
      'fields' => ['googlefonts', 'settings']
    ]);

    $this->addBehavior('Manager.Crudable');

    $this->crud
      ->addJsFiles(['/website/js/websites.js'])
      ->addFields([
        'title' => 'Título',
        'subtitle' => 'Subtítulo',
        'domain' => 'Dominio',
        'description' => 'Descripción',
        'email' => 'Email',
        'local_business' => [
          'label' => 'Local Business JSON',
          'type' => 'text',
        ],
        'local_business_info' => [
          'label' => '',
          'type' => 'info',
          'template' => 'Website.fields.local_business_info',
        ],
        'mail_footer' => [
          'label' => 'Texto del pie de los emails',
          'help' => __d('admin', 'Texto que cierra los emails que se envían desde el web')
        ],
        'mail_legal' => [
          'label' => 'Texto legal de los emails',
          'help' => __d('admin', 'Texto legal que se coloca en los emails que se envían desde el web')
        ],
        'settings.phone' => [
          'type' => 'string',
          'label' => __d('admin', 'Teléfono'),
          'help' => __d('admin', 'El teléfono del web. Pueden ser varios separados por comas (,)')
        ],
        'date_post' => [
          'label' => __d('admin', 'Fecha de publicación'),
          'help' => __d('admin', "Edición avanzada, usando cadenas de la función PHP strftime")
        ],
        'date_post_short' => [
          'label' => __d('admin', 'Fecha de publicación (corta)'),
          'help' => __d('admin', "Edición avanzada, usando cadenas de la función PHP strftime")
        ],
        'date_event_punctual' => [
          'label' => __d('admin', 'Fecha de evento puntual'),
          'help' => __d('admin', "Edición avanzada, usando cadenas de la función PHP strftime")
        ],
        'date_event_permanent' => [
          'label' => __d('admin', 'Fecha de evento permanente'),
          'help' => __d('admin', "Edición avanzada, usando cadenas de la función PHP strftime")
        ],
        'date_event_permanent_expr' => [
          'label' => __d('admin', 'Fecha de evento permanente (expresión)'),
          'help' => __d('admin', "Edición avanzada, usando cadenas de la función PHP strftime")
        ],
        'settings.use_seo_title' => [
          'type' => 'boolean',
          'label' => __d('admin', 'Usar título de Metatag si lo hubiera'),
          'help' => __d('admin', "Usará el tíulo indicado en las Metatags en lugar del título de la página")
        ],
        'settings.google_analytics' => [
          'type' => 'string',
          'label' => __d('admin', 'Código de Google Analytics'),
          'help' => __d('admin', "Introduce aquí el código de Google Analytics. Es tipo UA-XXXXXXXX-X")
        ],
        'settings.google_analytics_4' => [
          'type' => 'string',
          'label' => __d('admin', 'Código de Google Analytics 4'),
          'help' => __d('admin', "Introduce aquí el código de Google Analytics. Es tipo G-XXXXXXXXXX")
        ],
        'settings.google_gtm' => [
          'type' => 'string',
          'label' => __d('admin', 'Google Tag Manager'),
          'help' => __d('admin', "Introduce aquí el código de Google Tag Manager. Es tipo GTM-XXXXXXX")
        ],
        'settings.google_maps' => [
          'type' => 'string',
          'label' => __d('admin', 'Introduce el código de API para Googlemaps'),
          'help' => __d('admin', "Introduce aquí el código de Google Maps")
        ],
        'section_title_format' => [
          'label' => __d('admin', 'Formato del título de sección'),
          'template' => 'Website.fields.section_title_format',
          'help' => __d('admin', "Puedes utilizar las siguientes claves de texto: %s, el nombre del web; %c, el nombre de la sección")
        ],
        'settings.disallow_robots' => [
          'type' => 'boolean',
          'label' => __d('admin', 'Esconder web para los buscadores'),
          'help' => __d('admin', "Marca esta opción si quieres que el web no se indexe en los buscadores")
        ],
        'item_title_format' => [
          'label' => __d('admin', 'Formato del título de un contenido'),
          'template' => 'Website.fields.item_title_format',
          'help' => __d('admin', "Puedes utilizar las siguientes claves de texto: %s, el nombre del web; %c, el nombre de la sección; %i, el título del contenido")
        ],
        'address' => [
          'translate' => false,
          'type' => 'ckeditor',
          'label' => __d('admin', 'Dirección de la empresa (calle, plaza, ciudad, teléfono...)'),
          'help' => __d('admin', "Indica la dirección de la empresa")
        ],
        'googlefonts' => [
          'label' => 'Google Fonts',
          'template' => 'Website.fields.googlefonts',
        ],
        'theme' => __d('admin', 'Tema'),
        'settings.facebook' => [
          'type' => 'string',
          'label' => 'Facebook',
          'help' => __d('admin', 'Indica la dirección de la página de Facebook')
        ],
        'settings.youtube' => [
          'type' => 'string',
          'label' => 'Youtube',
          'help' => __d('admin', 'Indica la dirección de la página de Youtube')
        ],
        'settings.twitter' => [
          'type' => 'string',
          'label' => 'Twitter',
          'help' => __d('admin', 'Indica la dirección de la página de Twitter')
        ],
        'settings.google' => [
          'type' => 'string',
          'label' => 'Google+',
          'help' => __d('admin', 'Indica la dirección de la página de Google+')
        ],
        'settings.linkedin' => [
          'type' => 'string',
          'label' => 'LinkedIn',
          'help' => __d('admin', 'Indica la dirección de la página de LinkedIn')
        ],
        'settings.pinterest' => [
          'type' => 'string',
          'label' => 'Pinterest',
          'help' => __d('admin', 'Indica la dirección de la página de Pinterest')
        ],
        'settings.instagram' => [
          'type' => 'string',
          'label' => 'Instagram',
          'help' => __d('admin', 'Indica la dirección de la página de Instagram')
        ],
        'settings.ivoox' => [
          'type' => 'string',
          'label' => 'Ivoox',
          'help' => __d('admin', 'Indica la dirección de la página de Ivoox')
        ],
        'settings.tripadvisor' => [
          'type' => 'string',
          'label' => 'TripAdvisor',
          'help' => __d('admin', 'Indica la dirección de la página de TripAdvisor')
        ],
        'settings.vimeo' => [
          'type' => 'string',
          'label' => 'Vimeo',
          'help' => __d('admin', 'Indica la dirección de la página de Vimeo')
        ],
        'settings.issuu' => [
          'type' => 'string',
          'label' => 'Issuu',
          'help' => __d('admin', 'Indica la dirección de la página de ISSUU')
        ],
        'settings.wikiloc' => [
          'type' => 'string',
          'label' => 'Wikiloc',
          'help' => __d('admin', 'Indica la dirección de la página de Wikiloc')
        ],
        'settings.blog' => [
          'type' => 'string',
          'label' => 'Blog',
          'help' => __d('admin', 'Indica la dirección del blog del web')
        ],
        'settings.without_add_categories' => [
          'type' => 'boolean',
          'label' => 'No dar posibilidad de añadir categorías desde la edición',
          'help' => __d('admin', 'No dar posibilidad de añadir categorías desde la edición')
        ],
        'settings.spotify' => [
          'type' => 'string',
          'label' => 'Spotify',
          'help' => __d('admin', 'Indica la dirección de la página de Spotify')
        ],
      ])
      ->addIndex('index', [
        'fields' => [
          'title',
        ],
        'actionButtons' => []
      ])
      ->setName([
        'singular' => __d('admin', 'Configuración de web'),
        'plural' => __d('admin', 'Configuración de web'),
      ])
      ->addView('create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'subtitle',
                  'domain',
                  'description',
                  'email',
                  'settings.phone',
                  'section_title_format',
                  'item_title_format',
                  'googlefonts'
                ]
              ]
            ]
          ]
        ]
      ]);

    $elements = [
      'title',
      'subtitle',
      'domain',
      'description',
      'email',
      'settings.phone',
      'address',
      'settings.use_seo_title',
      'section_title_format',
      'item_title_format',
      'settings.disallow_robots',
    ];

    // No es un web multisite
    if (Configure::read('App.multisite') == false) {
      unset($elements[array_search('domain', $elements)]);
    }

    $this->crud->addView('update', [
      'columns' => [
        [
          'title' => __d('admin', 'General'),
          'key' => 'general',
          'box' => [
            [
              'key' => 'general',
              'elements' => $elements
            ],
            [
              'title' => __d('admin', 'Formato de fechas'),
              'elements' => [
                'date_post',
                'date_post_short',
                'date_event_punctual',
                'date_event_permanent',
                'date_event_permanent_expr',
              ]
            ]
          ]
        ],
        [
          'title' => __d('admin', 'Aspecto'),
          'box' => [
            [
              'elements' => [
                // 'googlefonts',
                'theme'
              ]
            ]
          ]
        ],
        [
          'title' => __d('admin', 'Emails'),
          'box' => [
            [
              'elements' => [
                'mail_footer',
                'mail_legal',
              ]
            ]
          ]
        ],
        [
          'title' => __d('admin', 'Edición'),
          'box' => [
            [
              'elements' => [
                'settings.without_add_categories'
              ]
            ]
          ]
        ],
        [
          'title' => __d('admin', 'Redes sociales'),
          'box' => [
            [
              'elements' => [
                'settings.facebook',
                'settings.twitter',
                'settings.google',
                'settings.youtube',
                'settings.vimeo',
                'settings.linkedin',
                'settings.instagram',
                'settings.pinterest',
                'settings.ivoox',
                'settings.tripadvisor',
                'settings.issuu',
                'settings.wikiloc',
                'settings.blog',
                'settings.spotify',
              ]
            ]
          ]
        ],
        [
          'title' => __d('admin', 'Servicios externos'),
          'box' => [
            [
              'elements' => [
                'settings.google_analytics',
                'settings.google_analytics_4',
                'settings.google_gtm',
                'settings.google_maps',

              ]
            ]
          ]
        ],
        [
          'title' => __d('admin', 'Local Business'),
          'box' => [
            [
              'elements' => [
                'local_business_info',
                'local_business',
              ]
            ]
          ]
        ]
      ],
      'actionButtons' => []
    ]);

    $this->addBehavior('Seo.Seo');

    $this->crud->addFields([
      'settings.seo_author' => [
        'type' => 'string',
        'label' => __d('admin', 'Autor')
      ]
    ]);

    $this->crud->addElementsToBox(['create', 'update'], 'seo', 'seo', [
      'settings.seo_author'
    ], 'seos.description_seo');
  }


  /**
   * Setea el sitio actual en la configuración de Website
   */
  public function setSite()
  {
    // Si el sitio ya está seteado, retornamos
    if (Website::isSeted()) {
      return;
    }

    if (!Configure::read('App.multisite')) {
      $site = $this->find()->first();
    } else {
      $site = $this->find()->where([
        'Sites.domain' => env('HTTP_HOST')
      ])->first();
    }

    if ($site) {
      // Event
      $event = new Event('Store.Model.Sites.beforeSetSite', $this, [
        $site,
      ]);

      $this->eventManager()->dispatch($event);
      Website::set($site);
    }
  }

  /**
   * Crea un site por defecto
   * 
   * @return Entity | false El site creado o false si no se ha creado
   */
  public function createDefaultSite()
  {
    // Campos traducibles
    $translates = [
      'title' => 'Mi sitio web',
      'subtitle' => 'Un buen sitio web',
      'description' => 'Descripción de mi sitio web',
      'section_title_format' => '%c - %s',
      'item_title_format' => '%i - %c - %s'
    ];

    $data = $translates;

    $langs = Lang::iso3();

    foreach ($langs as $locale => $name) {
      foreach ($translates as $key => $value) {
        $data[$locale][$key] = $value;
      }
    }

    $entity = $this->getNewEntity($data);

    return $this->save($entity);
  }
}
