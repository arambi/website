<?php
namespace Website\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Site Entity.
 */
class Site extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
    'slug' => true,
    'domain' => true,
    'title' => true,
    'description' => true,
    'subtitle' => true,
    'settings' => true,
    'email' => true,
    'salt' => true,
    'logo' => true,
    'theme' => true,
    'favicon' => true,
    'categories' => true,
    'contents' => true,
    'sections' => true,
    'item_title_format' => true,
    'section_title_format' => true,
    'googlefonts' => true,
    'seo' => true,
    'address' => true
  ];
}
