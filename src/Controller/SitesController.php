<?php
namespace Website\Controller;

use Cake\Core\Configure;
use Cake\Routing\Router;
use Section\Seo\Sitemap;
use Cake\Network\Http\Client;
use Website\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Sites Controller
 *
 * @property \Website\Model\Table\SitesTable $Sites
 */
class SitesController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');

    if( isset( $this->Auth))
    {
      $this->Auth->allow();
    }

  }

  public function robots()
  {
    $this->viewBuilder()->setLayout( 'ajax');
    $this->setResponse( $this->getResponse()->withType( 'text'));

    $this->loadModel( 'Section.Sections');
    $sections = $this->Sections->find()
        ->where([
          'Sections.published' => true,
          'Sections.sitemap_exclude' => true,
        ]);
    
    $Sitemap = new Sitemap();
    $disallows = $Sitemap->getDisallows();
    $this->set( compact( 'disallows'));
  }
}
