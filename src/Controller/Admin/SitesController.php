<?php
namespace Website\Controller\Admin;

use Website\Controller\AppController;
use Manager\Controller\CrudControllerTrait;
use Cake\Core\Configure;
use Cake\Network\Http\Client;

/**
 * Sites Controller
 *
 * @property \Website\Model\Table\SitesTable $Sites
 */
class SitesController extends AppController
{
  use CrudControllerTrait;


  private function __setGoogleFonts()
  {
    $http = new Client();
    $response = $http->get( 'https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBDHSX-TZ-WsD42_4CI0WET8fSSLq62E_Y');
    $body = json_decode( $response->body(), true);
    $items = $body ['items'];

    $fonts = [];

    foreach( $items as $item)
    {
      $fonts [] = $item ['family'];
    }

    $this->Sites->crud->addConfig( ['googlefonts' => $fonts]);
  }
/**
 * Se encarga de setear el id en el caso de que el web no sea multisite y no haya $id como parámetro
 * 
 * @param  integer | false $id
 * @return void
 */
  protected function _beforeUpdate( $id = false)
  {
    // $this->__setGoogleFonts();

    if( Configure::read( 'App.multisite') == false && $id == false)
    {
      $site = $this->Sites->find()->select( 'Sites.id')->first();

      if( empty( $site))
      {
        $site = $this->Sites->createDefaultSite();
      }

      $this->request->data( 'id', $site->id);
    }
  }
}
