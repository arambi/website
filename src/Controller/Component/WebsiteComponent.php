<?php
/**
 * Componente usado en los Controllers cuyo model principal tiene relación con Site 
 *
 * @package Website.Controller.Component 
 */
 
namespace Website\Controller\Component;


use Cake\Configure\Engine\IniConfig;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Utility\ClassRegistry;
use Cake\Utility\Inflector;
use Cake\Event\Event;
use Website\Lib\Website;

class WebsiteComponent extends Component 
{  

/**
 * Componentes a utilizar
 *
 * @var array
 */
  public $components = array( 'Session');
  
/**
 * Callback
 * Al inicializar el Component
 * 
 * Cuando se esté editando un sitio, se le setea al model principal las siguientes propiedades
 * Model::restrictSite
 * Model::sitePermalink
 * Estas propiedaes servirán a SitableBehavior para que al guardar un nuevo registro se coloque automáticamente site_id
 *
 * @param object $controller 
 */
  function initialize( array $config)
  {        
    
  }

  public function beforeFilter( Event $event)
  {
    $this->Controller = $event->subject();
    $this->Controller->viewBuilder()->theme( Website::get()->theme);
    // $this->setAdminSite();
    // $this->setFrontDomain( $this->Controller->request->host());
  }

  
  public function startup( Event $event)
  {

  }


/**
 * Setea en la configuración el site actual que se está editando
 *
 * @return void
 */
  function setAdminSite()
  {
    if( !isset( $this->Controller->request->params ['admin']))
    {
      return;
    }
    
    if( !empty( $this->Controller->request->params ['site']))
    {
      $this->Controller->loadModel( 'Site');

      $site = ClassRegistry::init( 'Site')->find( 'first', array(
          'conditions' => array(
              'Site.slug' => $this->Controller->request->params ['site']
          )       
      ));
      
      Configure::write( 'Website.current', $site);
    }
  }
  


/**
 * Devuelve el nombre del dominio dada la columna 'domain' del model Site
 *
 * @param string $domain la columna 'domain' del model Site
 * @return string El nombre del dominio
 * @since Shokesu 0.1
 */
  function hostName( $domain)
  {
    return Domain::hostName( $domain);
  }
  
/**
 * Verifica si un dominio existe en Site
 *
 * @param string $domain 
 * @return mixed El id del dominio si existe o false si no existe
 */
  public function getDomain( $domain)
  {
    $site = TableRegistry::get( 'Website.Sites')->find()->where([
      'Sites.domain' => $domain
    ])->first();

    return $site;
  }
  
/**
 * Redirige a la página principal del website, dado el id del website
 *
 * @param integer $site_id 
 * @return void
 */
  public function redirectToSite( $site_id)
  {
    $domain = $this->getDomainName( $site_id, true);
    $this->Controller->redirect( $domain);
  }

  public function getDomainName( $site_id, $http = false)
  {
    $this->Controller->loadModel( 'Site');
    
    $site = $this->Controller->Site->find( 'first', array(
        'conditions' => array(
            'Site.id' => $site_id
        ),
        'recursive' => -1,
        'fields' => array(
            'Site.domain'
        )
    ));
    
    $host = $this->hostName( $site ['Site']['domain']);
    
    if( $http)
    {
      $host = 'http://'. $host;
    }
    
    return $host;
  }
  
 
}