<?php

namespace Website\Routing\Filter;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Event\EventManagerTrait;
use Cake\ORM\TableRegistry;
use Cake\Routing\DispatcherFilter;
use Cake\Routing\Router;
use User\Auth\Access;
use Website\Lib\Website;

class LocalBusinessFilter extends DispatcherFilter
{

    use EventManagerTrait;


    public function beforeDispatch(Event $event)
    {
    }

    public function afterDispatch(Event $event)
    {
        $request = $event->data['request'];

        if ($request->getParam('prefix') != 'admin') {
            $this->_injectJson($event->data['response']);
        }
    }

    protected function _injectJson($response)
    {
        if (strpos($response->type(), 'html') === false) {
            return;
        }

        $json = $this->getJson();

        if (empty($json)) {
            return;
        }

        $body = $response->body();
        $pos = strpos($body, '</html>');

        $body = substr($body, 0, $pos) . $json . substr($body, $pos);
        $response->body($body);
    }

    private function getJson()
    {
        $json = Website::get('local_business');
        return $json;
    }
}
