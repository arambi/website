angular.module('admin')
  .directive( 'cfSeoTitles', function(){
    var titles = [
      {
        key: '%s',
        title: '<span class="badge">Nombre del web</span>'
      },
      {
        key: '%c',
        title: '<span class="badge">Nombre de la sección</span>'
      },
      {
        key: '%i',
        title: '<span class="badge">Nombre del item</span>'
      }
    ];

    function changeValues( text, availables){
      if( !text){
        return;
      }

      for( var i = 0; i < titles.length; i++){
        if( availables.indexOf( titles [i].key) > -1){
          text = text.replace( titles[i].key, titles[i].title);
        }
      }

      return text;
    }

    return {
      restrict: 'A',
      scope: true,
      link: function( scope, element, attrs){
        var availables = attrs.cfSeoTitles.split( ',');
        scope.$watch( attrs['ngModel'], function( newValue, oldValue){
          element.html( changeValues( newValue, availables));
        })
      }
    }
  })
;



